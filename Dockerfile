FROM alpine:3.7

MAINTAINER Paul Jang @qgp9

ADD Gemfile .
#ADD Gemfile.lock .
RUN apk add --update --no-cache bash ruby ruby-bundler ruby-json ruby-rake git nodejs \
                build-base ruby-dev zlib-dev \
        && bundler install --system \
        && apk del build-base ruby-dev zlib-dev \
        && rm -rf root/.bundle/
#/usr/lib/ruby/gems/2.4.0/cache/

